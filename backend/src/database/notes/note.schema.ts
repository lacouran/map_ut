import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import mongoose = require('mongoose');

export type NoteDocument = Note & Document;

@Schema()
export class Note {
  @Prop()
  problem_caution: boolean;

  @Prop()
  comment: string

  @Prop()
  pseudo: string

  @Prop()
  voisins: number

  @Prop()
  general: number

  @Prop()
  proprio: number

  @Prop({type: mongoose.Schema.Types.ObjectId, ref: 'Place'})
  place: any
}

export const NoteSchema = SchemaFactory.createForClass(Note);
