import { IsString, IsBoolean } from 'class-validator';

export class CreateNoteDto {

  @IsBoolean()
  problem_caution: boolean;

  @IsString()
  comment: string

  @IsString()
  pseudo: string

  voisins: number

  general: number

  proprio: number

  place : any
}
