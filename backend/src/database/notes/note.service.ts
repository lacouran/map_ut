import { Model } from 'mongoose';
import { Injectable, Inject } from '@nestjs/common';
import { Note, NoteDocument } from './note.schema';
import { CreateNoteDto } from './note.dto';
import mongoose = require('mongoose');

@Injectable()
export class NoteService {
  constructor(
    @Inject('NOTE_MODEL')
    private noteModel: Model<Note>,
  ) {}

  private forgeData(listcomments : Note[],idlogement:string) {
    const averages = {
      voisins: 0,
      general: 0,
      proprio: 0,
      problem_caution: 0
    };

    for (const comment of listcomments) {
      averages.voisins += comment.voisins;
      averages.general += comment.general;
      averages.proprio += comment.proprio;

      if (comment.problem_caution) {
        averages.problem_caution++;
      }
    }

    const output = {
      "problem_caution": (averages.problem_caution > listcomments.length/2),
      "notes": [
        {
          "type": "noteGeneral",
          "numbervotant": listcomments.length,
          "moyenne": averages.general / listcomments.length
        },
        {
          "type": "noteProprio",
          "numbervotant": listcomments.length,
          "moyenne": averages.proprio / listcomments.length
        },
        {
          "type": "noteVoisin",
          "numbervotant": listcomments.length,
          "moyenne": averages.voisins / listcomments.length
        }
      ],
      "commentaires": listcomments,
      "id": idlogement
    }

    return output;
  }

  async create(createNoteDto : CreateNoteDto) : Promise<string> {
    const createdNote = new this.noteModel(createNoteDto);

    createdNote.save();

    return "ok";
  }

  async findComments(idlogement:string) : Promise<any> {
    return this.forgeData(await this.noteModel.find({
      place: new mongoose.Types.ObjectId(idlogement)
    }).exec(),idlogement);
  }
}
