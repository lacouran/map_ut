import { Controller, Get, Post, Body, Put, Query, BadRequestException, Headers, UnauthorizedException } from '@nestjs/common';
import { PlaceService } from './places/place.service';
import { CreatePlaceDto } from './places/place.dto';
import { CreateNoteDto } from './notes/note.dto';
import { NoteService } from './notes/note.service';
import { AuthService } from './auth/auth.service';
import mongoose = require('mongoose');

@Controller()
export class DatabaseController {
  constructor(
    private readonly placeService: PlaceService,
    private readonly noteService: NoteService,
    private readonly authService: AuthService
  ) {}

  @Get("allplaces")
  allplaces(): any {
    return this.placeService.findAll();
  }

  @Post("addplace")
  addplace(@Body() data): any {
    const createPlaceDto = new CreatePlaceDto;

    createPlaceDto.formatted_address = data.place.formatted_address;
    createPlaceDto.geometry = data.place.geometry;
    createPlaceDto.name = data.place.name;
    createPlaceDto.loue = data.place.loue;

    createPlaceDto.place_id = data.place.place_id;
    createPlaceDto.price = data.place.price;
    createPlaceDto.rating = data.place.rating;
    createPlaceDto.types = data.place.type;

    createPlaceDto.archived = false;

    return this.placeService.create(createPlaceDto);
  }

  @Put("archive")
  archive(@Body() data): any {
    this.placeService.archive(data._id);
    return "ok";
  }

  @Put("editplace")
  editplace(@Body() data): any {
    return this.placeService.edit(data.place);
  }

  @Post("newplace")
  async newplace(@Body() data) : Promise<any> {
    if (data.adress && data.nom && data.places && data.prix && data.types && data.equipements) {
      const createPlaceDto = new CreatePlaceDto;

      createPlaceDto.formatted_address = data.adress;
      createPlaceDto.name = data.nom;
      createPlaceDto.loue = (data.places > 0 ? {
        "loue":true,
        "place_restant":data.places
      }:{
        "loue":false,
        "place_restant":0
      });
      createPlaceDto.price = data.prix;
      createPlaceDto.types = data.types;
      createPlaceDto.equipements = data.equipements;

      return {response:await this.placeService.create(createPlaceDto)};
    } else {
      throw new BadRequestException;
    }
  }

  @Post("newcomment")
  async newcomment(@Body() data, @Headers() heads) : Promise<any> {
    if (data.comment && data.pseudo && data.voisins && data.general && data.proprio && data.place) {
      const user = this.authService.getuser(heads.authorization);

      if (user) {
        const createNoteDto = new CreateNoteDto;

        createNoteDto.comment = data.comment;
        createNoteDto.problem_caution = data.problem_caution;
        createNoteDto.pseudo = user.name;
        createNoteDto.voisins = data.voisins;
        createNoteDto.general = data.general;
        createNoteDto.proprio = data.proprio;
        createNoteDto.place = new mongoose.Types.ObjectId(data.place);

        this.noteService.create(createNoteDto);
      } else {
        throw new UnauthorizedException;
      }

    } else {
      throw new BadRequestException;
    }
  }

  @Get("comments")
  async comments(@Query() data) {
    return {res : this.noteService.findComments(data.id)};
  }

  @Get("logbynote")
  async logByNote(@Query() data) : Promise<any> {
    return this.placeService.findByNote(data.note);
  }

  @Post("login")
  async login(@Body() data) {
    const logged = await this.authService.login(data.username,data.password);

    if (logged) {
      console.log("logged : "+logged);
      return {res : logged};
    } else {
      throw new BadRequestException;
    }
  }

  @Get("logout")
  async logout(@Headers() heads) {
    this.authService.logout(heads.authorization);

    return true;
  }
}
