import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import mongoose = require('mongoose');

export type PlaceDocument = Place & Document;

@Schema()
export class Place {
  @Prop()
  formatted_address: string;

  @Prop({type: {location : {lat : Number, lng : Number}}})
  geometry: any;

  @Prop()
  name: string;

  @Prop({type: {loue : Boolean, place_restant : Number}})
  loue: any;

  @Prop()
  place_id: string;

  @Prop()
  price: number;

  @Prop()
  rating: number;

  @Prop()
  types: string[];

  @Prop()
  archived: Boolean;

  @Prop({type:Object})
  equipements: any;

  @Prop()
  same_pos: any[]
}

export const PlaceSchema = SchemaFactory.createForClass(Place);
