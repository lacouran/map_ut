import { Connection } from 'mongoose';
import { PlaceSchema } from './place.schema';

export const placeProviders = [
  {
    provide: 'PLACE_MODEL',
    useFactory: (connection: Connection) => connection.model('Place', PlaceSchema),
    inject: ['DATABASE_CONNECTION'],
  },
];
