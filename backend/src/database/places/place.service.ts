import { Model } from 'mongoose';
import { Injectable, Inject } from '@nestjs/common';
import { Place, PlaceDocument } from './place.schema';
import { CreatePlaceDto } from './place.dto';
import { Note, NoteDocument } from './../notes/note.schema';
import { CreateNoteDto } from './../notes/note.dto';
import { Client } from "@googlemaps/google-maps-services-js";
import mongoose = require('mongoose');

const client = new Client({});

@Injectable()
export class PlaceService {
  constructor(
    @Inject('PLACE_MODEL')
    private placeModel: Model<Place>,
    @Inject('NOTE_MODEL')
    private noteModel: Model<Note>
  ) {}

  private getAllPosID (pos) {
    const same_pos = [];

    for (const p of pos) {
      same_pos.push(p._id);
    }

    return same_pos;
  }

  async create(createPlaceDto: CreatePlaceDto) : Promise<string> {
    //const geocoder = client.elevation()
    const placeid = await client.geocode({params:{
      address:createPlaceDto.formatted_address,
      key:process.env.API_KEY
    }}).then(async (res) => {
      createPlaceDto.geometry = {location: res.data.results[0].geometry.location};
      console.log(res.data.results[0])
      return this.placeModel.find({ "geometry.location" : res.data.results[0].geometry.location})
      .clone().exec().then((placesDoc)=>{

        const same_pos = this.getAllPosID(placesDoc);
        const createdPlace = new this.placeModel(createPlaceDto);

        for (const place of placesDoc) {
          place.same_pos = same_pos;

          place.save((err)=>{
            if (err) {
              console.log(err);
            }
          });
        }

        createdPlace.same_pos = same_pos;

        return createdPlace.save();
      });
    }).then((res)=>{
      return res._id.toString();
    }).catch((err)=>{
      console.log(err);

      return "";
    });

    console.log(placeid);
    return placeid;
  }

  async findAll(): Promise<Place[]> {
    return this.placeModel.find().exec();
  }

  async archive(id : string) {
    this.placeModel.findOne({ _id : id}, function(err, placeDoc) {
      if (err) {
        // TODO: Handle the error!
        console.log(err);
      }

      if (!placeDoc) {
        console.log("Le document n'a pas été trouvé");
      } else {
        placeDoc.archived = !placeDoc.archived;
        placeDoc.save(function (err) {
          if (err) {
              // TODO: Handle the error!
              console.log(err);
          }
        });
      }
    })
  }

  async edit(place : any) {
    this.placeModel.findOne({ _id : place._id}, function(err, placeDoc) {
      if (err) {
        console.log(err);
      }

      if (!placeDoc) {
        console.log("Le document n'a pas été trouvé");
      } else {
        placeDoc.formatted_address = place.formatted_address;
        placeDoc.geometry = place.geometry;
        placeDoc.name = place.name;
        placeDoc.loue = place.loue;

        placeDoc.place_id = place.place_id;
        placeDoc.price = place.price;
        placeDoc.rating = place.rating;
        placeDoc.types = place.type;

        placeDoc.save(function (err) {
          if (err) {
              // TODO: Handle the error!
              console.log(err);
           }
        });
      }
    });
  }

  async findByNote(note : number) : Promise<any> {
    let toFind = [];

    return await this.noteModel.aggregate([{
        $group: {
          _id: "$place",
          avgProp: {$avg:"$proprio"},
          avgGen: {$avg:"$general"},
          avgVoi: {$avg:"$voisins"}
        }
      }], function(err, res) {
      if (err) {
        console.log(err);
      } else if (!res) {
        console.log("Le document n'a pas été trouvé");
      } else {
        const result = res.filter((n)=>{
          return (n.avgProp + n.avgGen + n.avgVoi)/3 > note;
        });

        for (const elt of result) {
          toFind.push(elt._id);
        }
      }
    }).then(()=>{
      return this.placeModel.find({
          '_id' : {
            $in: toFind
          }
        }, function(err,res) {
          if (err) {
            console.log(err);
          } else if (!res) {
            console.log("Le document n'a pas été trouvé");
          } else {
            return res;
          }
      }).clone();
    });
  }
}
