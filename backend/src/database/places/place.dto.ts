import { IsInt, IsString, IsObject, IsBoolean} from 'class-validator';

export class CreatePlaceDto {

  @IsString()
  formatted_address: string;

  @IsObject()
  geometry: any;

  @IsString()
  name: string;

  @IsObject()
  loue: any;

  @IsString()
  place_id: string;

  price: number;

  rating: number;

  @IsString()
  types: string[];

  @IsBoolean()
  archived: Boolean;

  @IsObject()
  equipements: any;

  same_pos: any[];
}
