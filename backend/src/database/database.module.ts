import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { databaseProviders } from './providers/database.providers';
import { PlaceService } from './places/place.service';
import { placeProviders } from './places/place.providers';
import { DatabaseController } from './database.controller';
import { NoteService } from './notes/note.service';
import { noteProviders } from './notes/note.providers';
import { AuthService } from './auth/auth.service';

@Module({
  imports: [MongooseModule.forRoot("mongodb://localhost/map_ut")],
  providers: [...databaseProviders,...placeProviders,...noteProviders,PlaceService,NoteService,AuthService],
  exports: [...databaseProviders],
  controllers: [DatabaseController],
})
export class DatabaseModule {}
