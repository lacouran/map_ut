import { Injectable, Inject } from '@nestjs/common';

@Injectable()
export class AuthService {
  constructor() {}
  //TODO remettre cette partie à jour avec la vrai méthode décrite dans la doc, et faire une gestion des utilisateurs et des sessions avec la bdd
  private accounts = [{
    username: "test1",
    password: "azertyuiop",
    name: "Kevin"
  },
  {
    username: "test2",
    password: "azertyuiop",
    name: "Pierre"
  },
  {
    username: "test3",
    password: "azertyuiop",
    name: "Michel"
  }];

  private sessions = {};

  private findAccount (username: string, password: string) : number {
    for (const account in this.accounts) {
      if (this.accounts[account].username === username && this.accounts[account].password === password) {
        return parseInt(account,10);
      }
    }
    return -1;
  }

  async login(username: string, password: string) {
    const account = this.findAccount(username,password);
    if (account >= 0) {
      const current = Date.now();

      this.sessions[current+""] = this.accounts[account];

      return current;
    }

    return false;
  }

  async logout(auth) {
    this.sessions[auth+""] = null;
  }

  getuser(auth) : any {
    return this.sessions[auth+""];
  }
}
