import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { join } from 'path';

const places = require(join(__dirname, '..', 'resources/places.json'));

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get("placedata")
  placedata(): any {
    return {
  			"formatted_address": "10 rue saint Joseph",
  			"geometry": {
  				"location": {
  					"lat": 49.41229,
  					"lng": 2.82145
  				}
  			},
  			"id": "1113d3dd7339f965caae39387dd808a9e877bc2e",
  			"name": "l'Agence",
  			"loue": {
  				"loue": false,
  				"place_restant": 5
  			},
  			"place_id": "ChIJj62e80jHwoARusJT4mjohWw",
  			"price": 2,
  			"rating": 4.6,
  			"types": [
  				"colocation",
          "etudiant"

  			]
  		};
  }
}
