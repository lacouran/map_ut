import { baseUrl } from './Config'
import { deleteItemAsync, getItemAsync, setItemAsync } from 'expo-secure-store'
import * as Permissions from 'expo-permissions'
import * as Notifications from 'expo-notifications'

const axios = require('axios')

const header = {
    headers: {
        "Content-Type": "application/json",
    }
}

function headerSecure (token) {
    return ({
        headers:{
            "Authorization": token,
        }
    })
}

async function callLogin (data,keepConnection) {
    const res = await axios.post(baseUrl + 'auth/login/', data, header)
    if (res.status === 200) {
        res.data.user.token = res.data.access;
        await setItemAsync('user', JSON.stringify(res.data.user))
        if (keepConnection) {
            await setItemAsync('tokens', JSON.stringify([res.data.access, res.data.refresh]))
            await setItemAsync('user_info', JSON.stringify(data))
        }
        return res.data.user
    } else {
        return null
    }
}

async function callLogout () {
    await deleteItemAsync('tokens')
    await deleteItemAsync('user')
    await deleteItemAsync('user_info')
}

async function callRefresh () {
    const user = await getItemAsync('user_info')
    return await callLogin(JSON.parse(user))
}

function renewLogin () {

}

async function manageExpoPushToken () {
    const expoToken = await getItemAsync('expo_token')
    console.log(expoToken)

    if (expoToken === null) {
        const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS)

        if (status === 'granted') {
            const notificationToken = await Notifications.getExpoPushTokenAsync()
            await setItemAsync('expo_token', notificationToken.data)
            console.log('token', notificationToken.data)
        }
    }
}

async function getRequest (url, token) {
    const header = headerSecure(`Bearer ${token}`)
    const res = await axios.get(url, header)
    return res.data
}

async function postRequest (url, token,data) {
    const header = headerSecure(`Bearer ${token}`)
    const res = await axios.post(url, data, header)
    return res.data
}

export { callLogout, callLogin, renewLogin,callRefresh, getRequest, postRequest, manageExpoPushToken }
