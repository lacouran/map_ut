import { useEffect, useMemo, useReducer, useState } from "react";
import { TextInput, View, Text, StyleSheet, Dimensions, ScrollView, TouchableOpacity, CheckBox, SectionList } from "react-native";
import { baseIPAddress, newPlacePath, RequestPaths } from "../../../constants/server";
import { SelectInput } from "../../components/form/select";
import {Grid, TextField, Card, CardContent, Typography} from "@mui/material";
import RatingForm from "../../components/form/rating";

export default function NewPlace()
{
    const cityList = ["Compiègne", "Margny", "Venette"]
    const typeList = ["Maison", "Appartement", "Studio"]
    const equipmentList = ["Wifi", "Canapé"]

    const [address, setaddress] = useState("");
    const [city, setcity] = useState("Compiègne");
    const [comp, setcomp] = useState("");
    const [name, setname] = useState("");
    const [placesRestantes, setplacesRestantes] = useState("");
    const [prix, setprix] = useState("");
    const [type, settype] = useState("");
    const [equip, updateEquip] = useReducer(reducingUpdate, equipmentList, reducingInit)

    const [isMainCity, setIsMainCity] = useState(true);
    const [isMainType, setIsMainType] = useState(true);

    const [customReload, setCustomReload] = useState(false)
    function Reload() { setCustomReload(!customReload) }
    const [placeAdded, setPlaceAdded] = useState(false)
    const [errorText, setErrorText] = useState("")

    //When place added
    const [placeId, setId] = useState(0)

    function createPlace()
    {
        if (!address || !city || !name || !prix || !type)
        {
            //If a necessary var is undefined, render a user friendly error
            setErrorText("Veuillez bien remplir tous les champs obligatoires.")
            return
        }

        let place =
        {
            //Création de l'objet JSON à envoyer
            "address": [address, city].join(", "),
            "name": name,
            "place_restant": placesRestantes,
            "price": prix,
            "types": type,
            "equipment" : equip
        }

        fetch(baseIPAddress + RequestPaths.new_place, {
            //Appel du back
            method: "post",
            body: JSON.stringify(place),
            mode: "no-cors"
        }).then(res => {
            console.log("something")
            console.log(res)
            //En cas de réussite de création
            setPlaceAdded(true)
            setId(res.formData._id)
        }).catch(err => {
            //En cas d'échec, render error
            setErrorText("Erreur serveur, essayez de bien remplir tous les champs")
        })


    }


    const style = StyleSheet.create({
        input: {
            borderBottomWidth: 1,
            fontSize: 15,
            borderBottomColor: "grey",
            width: Dimensions.get('window').width * (1/2)
        },
        titles: {
            fontSize: 30,
            fontWeight: "bold",
            marginTop: 25,
            marginBottom: 15
        },
        grid: {
          marginLeft: 20,
          alignItems: 'center',
          justifyContent: 'center',
          display: 'flex'
        }
    })

    return (
      <ScrollView style={{ backgroundColor: "#efd510"}}>
      <Card style={{marginRight:30, marginLeft: 30}}>
        <CardContent>
          <Typography gutterBottom variant="h3" align="center">
            Ajouter un logement
          </Typography>
        <View style={{marginTop:20, backgroundColor:"white", height:"100%"}} >
          {errorText!=""? <Text>{errorText}</Text> : null}
            <Grid xs={12} item style={{marginLeft:20}} container spacing={1}>
              <Text style={style.titles}>Adresse</Text>
            </Grid>
            <Grid xs={12}  item style={{marginLeft:20}} container spacing={1}>
              <TextField required value={address} placeholder="Adresse" onChange={e => setaddress(e.target.value)}/>
              <TextField style={{marginLeft:3}} onChange={e => setcomp(e.target.value)} value={comp} placeholder="Complément"/>
            </Grid>
            <Grid item style={{marginLeft:20}} container spacing={1}>
              <SelectInput style={{fontSize:style.input.fontSize, marginTop:10, marginBottom:10}} SetIsMain={setIsMainCity} useOther={true} onChangeText={setcity} value={city} list={cityList}/>
              {isMainCity ?
                <Grid item container spacing={1}>
                  <TextField placeholder="Autre" onChange={e => setcity(e.target.value)} value={city}/>
                </Grid>
                : null}
            </Grid>
            <Grid xs={12} item style={{marginLeft:20}} container spacing={1}>
              <Text style={style.titles}>Nom</Text>
            </Grid>
            <Grid xs={12} item style={{marginLeft:20}} container spacing={1}>
              <TextField required onChange={e => setname(e.target.value)} value={name} placeholder="Nom"/>
            </Grid>
            <Grid xs={12} item style={{marginLeft:20}} container spacing={1}>
              <Text style={style.titles}>Places restantes</Text>
            </Grid>
            <Grid xs={12} item style={{marginLeft:20}} container spacing={1}>
              <TextField required onChange={e => setplacesRestantes(e.target.value)} value={placesRestantes} placeholder="Places restantes"/>
            </Grid>
            <Grid xs={12} item style={{marginLeft:20}} container spacing={1}>
              <Text style={style.titles}>Prix</Text>
            </Grid>
            <Grid xs={12} item style={{marginLeft:20}} container spacing={1}>
              <TextField required onChange={e => setprix(e.target.value)} value={prix} placeholder="Prix"/>
            </Grid>
            <Grid xs={12} item style={{marginLeft:20}} container spacing={1}>
              <Text style={style.titles}>Type de logement</Text>
            </Grid>
            <Grid xs={12} item style={{marginLeft:20}} container spacing={1}>
              <SelectInput style={{fontSize:style.input.fontSize, marginTop:10, marginBottom:10}} SetIsMain={setIsMainType} useOther={true} onChangeText={settype} value={type} list={typeList}/>
              {isMainType ?
                <Grid item container spacing={1}>
                  <TextField style={{...style.input, marginBottom:25}} placeholder="Autre" onChange={e => settype(e.target.value)} value={type}/>
                </Grid>
                : null}
            </Grid>
            <Grid xs={12} item style={{marginLeft:20}} container spacing={1}>
              <Text style={style.titles}>Meubles</Text>
            </Grid>
            <Grid xs={12} item style={{marginLeft:20}} container spacing={1}>
              <View>
                {equipmentList.map(item => {
                  return (
                    <View key={item} style={{flexDirection:"row"}}>
                      <CheckBox style={{marginBottom:10}} value={equip[item]} onValueChange={(val) => {updateEquip({"value": val, "item": item}); Reload()}} />
                      <Text>{item}</Text>
                    </View>
                  )
                })}
              </View>
            </Grid>
            <Grid style={{alignSelf: 'center'}}>
              {placeAdded ? null : <TouchableOpacity style={{borderRadius:1, backgroundColor:"black", padding:10, width:180 }} onPress={createPlace}>
                <Text style={{fontSize:16, color:"white", fontWeight:"bold", textAlign:"center"}}>Ajouter le logement</Text>
              </TouchableOpacity>}
            </Grid>
        </View>
        </CardContent>
      </Card>
      {placeAdded ? <RatingForm value={placeId}/> : null}
      </ScrollView>

    )
}


/**
 * Reducing functions
 */
function reducingInit(equipmentList)
{
    let dic = {}
    equipmentList.forEach(element => {
        dic[element] = false
    })
    return dic
}

function reducingUpdate(state, action)
{
    state[action.item] = action.value
    return state
}
