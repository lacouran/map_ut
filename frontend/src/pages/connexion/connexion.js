import { useContext, useState } from "react";
import { TextInput, Button, View, Text } from "react-native";
import { baseIPAddress, RequestPaths } from "../../../constants/server";
import { AuthUserContext } from "../../context/AuthContext";


export function Connexion()
{
    const [username, setUsername] = useState("");
    const [token, setToken] = useContext(AuthUserContext)

    function SendServer()
    {
        setToken("some token")

        let h = new Headers()
        h.append("Content-Type", "application/json")
        h.append("accept", "application/json")

        console.log(h.get("Content-Type"))

        fetch(baseIPAddress + RequestPaths.connexion, {
            method: "POST",
            body: JSON.stringify({ username : "test1", password : "azertyuiop"}),
            headers: h
        }).then(res => console.log(res.test)).catch(err => console.log(err))
    }

    return (
        <View>
            {token != null ? <Text>Connecté.e</Text> : null}
            <View style={{flexDirection:"row"}}>
                <Text>Username : </Text><TextInput value={username} onChangeText={setUsername}/>
            </View>
            <Button title="valider" onPress={SendServer}/>
        </View>
    )
}