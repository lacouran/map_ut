import React from "react";
import { View, Text, StyleSheet, Button,ScrollView} from "react-native";
import  colors from '../../../colors'
import styled from "styled-components/native";
import InsideRoom from '../../components/InsideRoom'
import ImageCarousel from '../../components/Carousel'
import Review from '../../components/review'
import { styles_com } from '../../../constants/styles';






export default function Detail (props) {
  const {route,navigation}=props

  console.log(route.params.room)
  const goTo = () => navigation.navigate("stackMap");

  return (
    <ScrollView
      contentContainerStyle={{ alignItems: 'center' }}
      style={{ flex: 1, width: '100%' }}
      showsVerticalScrollIndicator={false}
    >


      <Text style={styles_com.title}>{route.params.room.name}</Text>

      <ImageCarousel   />
      <View style={{width:'100%'}}>
      <InsideRoom {...props} />


          <Review/>

      <Button onPress={goTo} title={`Go to maps`} />
      </View>
    </ScrollView>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "azure",
    alignItems: "center",
    justifyContent: "center",
  },
  container2: {
    flex: 1,
    backgroundColor: "#F5F8FF",
    alignItems: "center",
    width:'100%',
    justifyContent: "center",
  },
  carousel: {



  },
});
