import {API_KEY} from '@env'
const MY_API_KEY = API_KEY
console.log(MY_API_KEY)
import React, { useEffect, useState } from 'react';
import { Platform } from 'react-native';
import { baseIPAddress, RequestPaths } from '../../../constants/server';
import  jsonFile  from '../../../public/places.json';

export default function CustomMap (props){
  const [places,setPlaces]  = useState(CreatePlaces())
  const [region, setRegion] = useState({
    latitude: 49.41273,
    longitude: 2.81853,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  });

  function CreatePlaces()
  {
    /**
     * Should ask server instead of reading the json file.
     * Creating places object by sorting habitation on the same position.
     */

    jsonFile.results.forEach((result) => {
      result.show = false; // eslint-disable-line no-param-reassign
    })

    fetch(baseIPAddress + RequestPaths.get_places, {
        //Appel du back
        method: "GET",
        mode: "no-cors"
    }).then(res => console.log(res)).catch(err => console.log(err))
    
    //Mofifying places list to have lists of buildings
    let toBeSorted = jsonFile.results
    let sorted = []
    while (toBeSorted.length != 0)
    {
        let result = toBeSorted.shift()
        if (result.same_pos == [])
        {
            sorted.push([result])
        }
        else
        {
            //Searching for all places in the list before adding them to the global places
            let apts = [result]
            let i = 0
            while (i < toBeSorted.length)
            {
                if (result.same_pos.includes(toBeSorted[i]._id))
                {
                    //Add the element to list of apt and remove it from toBeSorted
                    apts.push(toBeSorted.splice(i, 1)[0])
                }
                else
                {
                    i++
                }
            }
            //Add the list to global list
            
            sorted.push(apts)
        }
    }

    return sorted
  }

  // onChildClick callback can take two arguments: key and childProps
  const updateState = (key) => {

      const index = places.findIndex((e) => e[0].id === key);
      places[index][0].show = !places[index][0].show; // eslint-disable-line no-param-reassign
      setPlaces([...places])
  };
  
  const MapView = Platform.OS == "web" ? require('./mapViews/webView').default : require('./mapViews/androidView').default;
  return (<MapView places = {places} region = {region} updateState = {updateState} {...props} />)

}
