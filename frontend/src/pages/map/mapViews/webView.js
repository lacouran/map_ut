import GoogleMapReact from "google-map-react"
import { InfoWindow } from "../../../components/map/infoWindow";
import { useState } from "react";



// Marker component
const Marker = (props) => {
  const { place } = props

  const markerStyle = {
      border: '1px solid white',
      borderRadius: '50%',
      height: 10,
      width: 10,
      backgroundColor: place[0].show ? 'red' : 'blue',
      cursor: 'pointer',
      zIndex: 10,
    };

    return (
      <>
        <div style={markerStyle} />
        {place[0].show && <InfoWindow {...props} places = {place} />}
      </>
    );
  };

const MY_API_KEY = process.env.API_KEY

export default function WebMapView (props){
    const { places, navigation } = props

    const [customReload, setCustomReload] = useState(false)
    function Reload() { setCustomReload(!customReload) }

    const updateRendering = (key) => 
    {
        props.updateState(key)
        Reload()
    }

    return(
        <GoogleMapReact
            bootstrapURLKeys={{
                key: MY_API_KEY,
            }}
            defaultZoom={10}
            defaultCenter={[49.41273, 2.81853]}
            onChildClick={updateRendering}
            >
            {places.map((place) => {
                let firstPlace = place[0] //To access localisation
                return(
                    <Marker
                        key={firstPlace.id}
                        lat={firstPlace.geometry.location.lat}
                        lng={firstPlace.geometry.location.lng}
                        {...props}
                        place={place}
                    />
            )})}
        </GoogleMapReact>

    )
}
