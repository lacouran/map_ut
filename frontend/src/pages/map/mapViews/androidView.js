import MapView, { Callout } from "react-native-maps"
import Marker from "react-native-maps"
import { goTo, InfoWindow } from "../../../components/map/infoWindow"
import { Dimensions } from "react-native"
import { Text } from "react-native"

// export default function AndroidMapView (props) {
//   const {region, places} = props
//   console.log(places)
//   return (
//     <MapView
//       style = {{height : Dimensions.get("screen").height, width : Dimensions.get("screen").width}}
//       initialRegion = {region}
//     >
//       <MapView.Marker
//         coordinate = {{latitude : region.latitude, longitude : region.longitude}}
//         key = {0}
//       />
//     </MapView>
//   )
// }

export default function AndroidMapView (props) {
  const {region, places, navigation} = props
  return(
    <MapView
        provider = {MapView.PROVIDER_GOOGLE}
        style = {{height : Dimensions.get("screen").height, width : Dimensions.get("screen").width}}
        initialRegion={region}
    >

      {places.map((place) => {
        let firstPlace = place[0] //To access localisation
        firstPlace = place[0] //To access localisation
        return(
          <MapView.Marker
            coordinate = {{latitude : firstPlace.geometry.location.lat, longitude : firstPlace.geometry.location.lng}}
            key = {firstPlace.id}

          >
            <Callout onPress={() => goTo(navigation, firstPlace)}>
              <InfoWindow {...props} places={place}/>
            </Callout>
          </MapView.Marker>
        )})}

    </MapView>
  )
}
