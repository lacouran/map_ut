import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import CustomMap from './map'
import Detail from './details'

const Stack = createNativeStackNavigator()

export default function MapHome ({ navigation }) {
  return(
    <Stack.Navigator
      initialRouteName='stackMap'
      screenOptions={{ headerShown: false }}>
      <Stack.Screen name='stackMap'>
        {props => <CustomMap {...props} />}

      </Stack.Screen>
      <Stack.Screen name='vuDetails'  screenOptions={{ headerShown: false }}>
        {props => <Detail {...props} />}
      </Stack.Screen>

    </Stack.Navigator>
  )
}
