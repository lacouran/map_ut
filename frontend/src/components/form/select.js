import { View, Text } from "react-native"


export function SelectInput({
    list,
    onChangeText,
    style,
    value,
    useOther = false,
    otherStr = "Autre",
    SetIsMain = null
}) {
    useOther ? SetIsMain(!list.includes(value)) : null

    return (
        <View style={{...style, flexDirection:"row"}}>
            {(useOther ? [...list, otherStr] : list).map((item) => {
                const fontColor = value == item ? "red" : "black"
                return (
                    <Text
                        onPress={() => {onChangeText(item)}}
                        style={{color:fontColor, marginRight:15}}>
                            {item}
                    </Text>
                )
            })}
        </View>
    )
}
