import { useEffect, useState } from "react"
import {Text, TextInput, View, CheckBox, TouchableOpacity} from "react-native"
import { Button } from "react-native-elements";
import {Card, CardContent, Typography, TextField, Grid} from "@mui/material";
import Rating from '@mui/material/Rating';
import { baseIPAddress, RequestPaths } from "../../../constants/server";

// data :
// "commentaire" :
// "note" : [
//     "global" :
//     "voisins" :
//     "proprio" :
// ]}


export default function RatingForm({id}){
    const [comment, setComment] = useState("")
    const [globalRating, setGlobalRating] = useState("")
    const [neiboursRating, setNeiboursRating] = useState("")
    const [proprioRating, setProprioRating] = useState("")
    const [cautionPB, setCautionPB] = useState(false)

    const [info, setInfo] = useState({})

    useEffect(() => {
        loadInfo()
    }, [])

    function loadInfo ()
    {
        let dic = {"name": "Some name", "formatted_address": "Some address"}
        /**
         * Load place info from id fetching
         */


        setInfo(dic)
    }

    function sendServer()
    {
        /**
         * Send rating to server fetching new comment/rating
         * Link with a user ID
         */

        /**
         * If no error go to map? Or other page
         * If error diplay it (like if username is already used)
         */

        let rating = {
            "comment": comment,
            "problem_caution": cautionPB,
            "voisins": neiboursRating,
            "general": globalRating,
            "proprio": proprioRating,
            "place": id
        }

        //Base
        fetch(baseIPAddress + RequestPaths.new_rating, {
            //Appel du back
            method: "post",
            body: JSON.stringify(rating),
            mode: "no-cors"
        }).then(res => {
            console.log("something")
            console.log(res)
            //Go to maps?
        }).catch(err => {
            //En cas d'échec, render error
            setErrorText("Erreur serveur, essayez de bien remplir tous les champs")
        })
    }

    return (
      <View style={{backgroundColor: "#efd510"}}>
      <Card style={{marginRight:30, marginLeft: 30}}>
        <CardContent>
          <Typography gutterBottom variant="h3" align="center">
            Noter un logement
          </Typography>
        <View style={{marginTop:20, backgroundColor:"white", height:"100%"}}>
          <Typography gutterBottom variant="h6">
            Nom : {info["name"]}
          </Typography>
          <Typography gutterBottom variant="body1" style={{marginBottom:20}}>
            Adresse : {info["formatted_address"]}
          </Typography>
          <Typography variant="h6" component="legend">
            Note globale
          </Typography>
          <Rating rating={globalRating} onStartRating={setGlobalRating} />
          <Typography variant="h6" component="legend" style={{marginTop:10}}>
            Note des voisins
          </Typography>
          <Rating rating={neiboursRating} onStartRating={setNeiboursRating} />
          <Typography variant="h6" component="legend" style={{marginTop:10}}>
            Note du propriétaire ou de l'agence
          </Typography>
          <Rating rating={proprioRating} onStartRating={setProprioRating} />
          <Grid xs={12} item style={{marginTop:20, marginLeft:5}} container spacing={1}>
            <CheckBox value={cautionPB} onValueChange={setCautionPB}/><Text> Problème de caution</Text>
          </Grid>
          <Typography variant="h6" component="legend" style={{marginTop:10}}>
            Commentaire :
          </Typography>
          <TextField multiline={true} value={comment} onChange={e => setComment(e.target.value)} style={{marginBottom:10}} />

          <Grid style={{alignSelf: 'center'}}>
          <TouchableOpacity style={{borderRadius:1, backgroundColor:"black", padding:10, width:180 }} onPress={sendServer}>
            <Text style={{fontSize:16, color:"white", fontWeight:"bold", textAlign:"center"}}>Noter le logement</Text>
          </TouchableOpacity>
          </Grid>
        </View>
        </CardContent>
      </Card>
      </View>
    )
}
