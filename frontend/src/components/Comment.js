import React, { useState, useEffect } from "react";
import {
  View,
  Image,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native'
import Colors from '../../constants/Colors'
import Sizes from '../../constants/Sizes'
import { format } from "date-fns";
import Star from "./star";
import notes from "../../public/notes.json";


function createElements(n) {
  let elements = [];
  for (let i = 0; i < n; i++) {
    elements.push(<Star/>);
  }
  return elements;
}


const Comment = ({ date, content,pseudo,note }) => {
  console.log(note)
  var date_ = new Date(date);
  const [noteData,setNoteData]  = useState(null)
  useEffect(() => {
    setNoteData(note)

  }, [])
  const [showResults, setShowResults] = React.useState(false)
  const showNote = () => setShowResults(!showResults)

  return (
      <View  style={styles.container}>

      <View style={{ width: '100%', alignItems: 'flex-end' }}>
        <Text style={styles.date}>{format(date_, "YYY-MM-dd HH:mm")}</Text>
      </View>

        <View style={{flexWrap:'wrap'}}>
          <Text style={styles.pseudo}>{pseudo}</Text>
        </View>
        <View style={{ width: '100%' }}>
          <Text style={styles.title}>{content}</Text>
        </View>
        <View>
          <button onClick={showNote} >{showResults ? "Voir les notes" : "Cacher les notes"}</button>
          {showResults ? null : <>
            {
              note.map(function(item){
                return(
                  <>
                    <View style={styles.totalWrap}>
                      <Text>{item.type}</Text>
                      <View  style={{
                        flexDirection: "row",
                      }}>
                        {
                          createElements(item.notes)
                        }
                      </View>
                      <Text>{item.notes}</Text>
                    </View>

                  </>

                )

              })
            }



          </>}


        </View>
      </View>

  )
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    marginTop: 10,
    backgroundColor: 'rgba(255, 255, 255, 0.9)',
    borderRadius: 5,
    padding: 5,
    margin: 5,
    borderWidth: 2,
    borderColor: '#2533A8',
  },
  title: {
    color: Colors.darkGrey,
    fontSize: Sizes.h3,
    fontWeight: 'bold',
    marginLeft: 5
  },
   pseudo: {

     fontStyle: 'italic',
     color: Colors.black,
     fontWeight: 'bold',
     marginLeft: 1,
     alignItems: 'flex-start',
     backgroundColor:'rgb(255,192,0)',
     marginBottom:10,
     borderRadius:30
   },
  amountText: {
    fontSize: 16,
    color: "#595B71",
    textAlign: "center",
  },
  totalWrap: {
    marginTop: 20,
    marginBottom: 5,
    backgroundColor: "#F5F8FF",
    borderRadius: 40,
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "row",
    paddingHorizontal: 15,
    paddingVertical: 10,
  },
  date: {
    fontStyle: 'italic',
    color: Colors.black,
    marginRight: 5
  }
});

export default Comment;
