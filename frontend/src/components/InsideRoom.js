import React from 'react'
import { Entypo } from '@expo/vector-icons'
import { Button, ScrollView, StyleSheet, TouchableOpacity, View,Text } from 'react-native';
import { styles_com } from '../../constants/styles';
import BedIcon from '@mui/icons-material/Bed';
import NetworkWifi3BarOutlinedIcon from '@mui/icons-material/NetworkWifi3BarOutlined';
import MicrowaveIcon from '@mui/icons-material/Microwave';
export default function InsideRoom (props) {
  const {
    route,
    navigation
  } = props

  const room = route.params.room

  return (
    <>
      {room.inside &&
        <>
          <Text style={styles_com.title}>Equipements Disponibles</Text>
          <View style={{ flexDirection: "row",paddingTop :"150"}}>
          {room.inside.bed && <BedIcon fontSize="large"/>}
          {room.inside.wifi && <NetworkWifi3BarOutlinedIcon fontSize="large"/>}
          {room.inside.four && <MicrowaveIcon fontSize="large"/>}
          </View>
        </>

      }
    </>
  )

}



// const styles = StyleSheet.create({
//   container: {
//     container: {
//       flex: 1,
//       backgroundColor: "#FF0000",
//       alignItems: "center",
//       justifyContent: "center",
//     },
//   },
//   carousel: {
//
//   },
// });
