import { TouchableOpacity, Button, View, Text } from "react-native";


import useComponentVisible from '../../components/ComponentVisible'

export const goTo = (navigation, place) => navigation.navigate("vuDetails",{room : place});

export const InfoWindow = (props) => {
   const { places } = props
   const { ref, isComponentVisible } = useComponentVisible(true);

   return (
    isComponentVisible && (
      <View style={{flexDirection: "column"}}>
          {places.map(place => {
            return (
              <InfoWindowOneAddress {...props} place = {place} ref = {ref} />
            )
          })}
      </View>
    )
   )
}

const InfoWindowOneAddress = (props) => {
    const { place, navigation, ref } = props;

    return (
      <View ref={ref} style={{marginTop: 30}}>

        <TouchableOpacity>
          <View style={{width: 200, height: 100, backgroundColor: "white"}}>
            <Text style = {{fontSize: 16}}>
              {place.name}
            </Text>

            <View style = {{fontSize: 14}}>

              <Text style={{ fontSize: 14, color: 'grey' }}>
                {place.types[0]}
              </Text>

              <View style={{ flexDirection: "row"}}>
                <Text style = {{ color: 'orange' }}>
                  {String.fromCharCode(9733).repeat(Math.floor(place.rating))}
                </Text>


                <Text style = {{ color: 'lightgrey' }}>
                  {String.fromCharCode(9733).repeat(5 - Math.floor(place.rating))}
                </Text>
              </View>
              <Text style = {{ color: 'grey' }}>
                {place.rating}
              </Text>
                <Text style={{ fontSize: 14, color: 'green' }}>
                  {place.loue.loue ? 'disponible' : 'occupé'}
                </Text>




              <Button onPress={() => goTo(navigation, place)} title={`voir`} />
            </View>
          </View>
        </TouchableOpacity>

      </View>
    )

  };
