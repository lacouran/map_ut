import React from 'react'
import { Entypo } from '@expo/vector-icons'
import { TouchableOpacity } from 'react-native'

const GoBackButton = ({ navigation }) => (
    <TouchableOpacity
        onPress={() => navigation.goBack()}
        style={{
            position: 'absolute',
            bottom: 20,
            left: 20,
            backgroundColor: 'white',
            borderRadius: 100,
            borderWidth: 2,
            borderColor: '#F98DBA'
        }}
    >
        <Entypo name="chevron-left" size={48} color="pink" />
    </TouchableOpacity>
)

export default GoBackButton