import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View,  Animated, } from "react-native";
import Star from "./star"
import { styles_com } from '../../constants/styles';


import  notes  from '../../public/notes.json';
import Comment from './Comment';
// import jsonFile from '../../public/places.json';
import { PercentageBar } from './PercentageBar';



function createElements(n) {
  var elements = [];
  for (var i = 0; i < n; i++) {
    elements.push(<Star/>);
  }
  return elements;
}

export default function Review() {
  const [reviewData,setreviewData]  = useState({"notes":null})
  useEffect(() => {
    setreviewData(notes)

  }, [])
  return reviewData.notes && (
    <View style={styles.container}>
      <View style={styles.reviewContainer}>
        <Text style={styles_com.title}>Customer reviews</Text>


          {
            reviewData.notes.map(function(item){
              return(

                <>
                <View style={styles.totalWrap}>
                  <Text>{item.type}</Text>
                  <View  style={{
                    flexDirection: "row",
                  }}>
                    {
                      createElements(item.moyenne)
                    }
                  </View>
                  <Text>{item.moyenne}</Text>
                </View>

                  <Text style={styles.amountText}>{item.numbervotant} customer ratings</Text>
                </>

              )

            })
          }
        <View style={{ marginTop: 40 }}>
          <View style={styles.spacer}>
            <PercentageBar starText="voisins" percentage={84} />
          </View>
          <View style={styles.spacer}>
            <PercentageBar starText="4 star" percentage={9} />
          </View>
          <View style={styles.spacer}>
            <PercentageBar starText="3 star" percentage={4} />
          </View>
          <View style={styles.spacer}>
            <PercentageBar starText="2 star" percentage={2} />
          </View>
          <View style={styles.spacer}>
            <PercentageBar starText="1 star" percentage={1} />
          </View>
        </View>
      </View>
      <View>
      {reviewData.commentaires.map((element,index) => (
        <Comment
          key={index}
          date={element.comment.date}
          content={element.comment.data}
          pseudo={element.comment.pseudo}
          note={element.comment.notes}
        />
      ))}
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F5F8FF",
    alignItems: "center",
    justifyContent: "center",
  },
  reviewContainer: {
    backgroundColor: "#FFFFFF",
    borderRadius: 10,
    paddingHorizontal: 30,
    paddingVertical: 40,
    minWidth: "80%",
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 1.0,
    shadowRadius: 2,
    shadowColor: "rgba(193, 211, 251, 0.5)",
    elevation: 5,
  },
  totalWrap: {
    marginTop: 20,
    marginBottom: 5,
    backgroundColor: "#F5F8FF",
    borderRadius: 40,
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "row",
    paddingHorizontal: 15,
    paddingVertical: 10,
  },
  amountText: {
    fontSize: 16,
    color: "#595B71",
    textAlign: "center",
  },
  howWeCalculate: {
    fontSize: 15,
    color: "#2A5BDA",
    textAlign: "center",
  },
  spacer: {
    marginBottom: 14,
  },
  progressText: {
    width: 50,
    fontSize: 14,
    color: "#2A5BDA",
  },
  progressPercentText: { width: 40, fontSize: 14, color: "#323357" },
  progressMiddle: {
    height: 15,
    flex: 1,
    marginHorizontal: 10,
  },
  progressWrap: {
    backgroundColor: "#F5F8FF",
    borderRadius: 18,
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    padding: 2,
  },
  progressBar: {
    flex: 1,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: "#ffcc48",
    shadowOpacity: 1.0,
    shadowRadius: 4,
    backgroundColor: "#FFCC48",
    borderRadius: 18,
    minWidth: 5,
  },
});
