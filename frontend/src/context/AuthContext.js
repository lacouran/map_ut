import React, {createContext, useState} from 'react'

export const AuthUserContext = createContext();

export const ContextProvider = ({children}) => {
    const [token, setToken] = useState(null)

    return (
        <AuthUserContext.Provider value={[token, setToken]}>
            {children}
        </AuthUserContext.Provider>
    )
}

