import { StyleSheet } from 'react-native';

export const styles_com = StyleSheet.create({

  title: {
    fontWeight: "bold",
    fontSize: 20,
    color: "#323357",
    textAlign: "center",
  }
});
