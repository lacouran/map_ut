import React, {useEffect, useState, useContext} from 'react'
import { Button } from 'react-native-elements'
import { NavigationContainer } from '@react-navigation/native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { Ionicons } from '@expo/vector-icons'

import mapHome from './src/pages/map/MapHome'
import NewPlace from './src/pages/newPlace/newPlace'
import { ContextProvider } from './src/context/AuthContext'
import { Connexion } from './src/pages/connexion/connexion'
import { AuthUserContext } from './src/context/AuthContext'


const Tab = createBottomTabNavigator()
const tabBarIconConfig = {
  Home: 'newspaper-outline',
  maps: 'map',
  logement: 'ios-add-circle-sharp',
}

export default function App() {

  return (
    <ContextProvider><ContextedApp /></ContextProvider>
  );
}

function ContextedApp() {
  const [connected, setConnected] = useState(false)
  const [token, setToken] = useContext(AuthUserContext)
  console.log(token)

  return (
    <NavigationContainer>
      <Tab.Navigator
        initialRouteName='Home'
        screenOptions={({ route }) => ({
          //Icons
          tabBarIcon: ({ focused, color, size }) =>
            <Ionicons name={tabBarIconConfig?.[route.name]} size={size} color={color} />,
          //Style
          tabBarActiveTintColor: 'blue',
          tabBarInactiveTintColor: 'gray',
          headerShown: false
        })}
      >
        {(token == null) ?
        <Tab.Screen name="Home" component={Connexion} /> :
        <Tab.Screen name="maps" component={mapHome} />
        } 
        {(token != null) ? 
        <Tab.Screen name="logement" component={NewPlace} /> : null}
        {(token != null) ? 
        <Tab.Screen name="account" component={Logout} /> : null}
      </Tab.Navigator>
    </NavigationContainer>
  )
}

function Logout() {
  const [token, setToken] = useContext(AuthUserContext)

  return (
    <Button style={{marginTop:50}} title="Disconnect" onPress={() => {setToken(null)}} />
  )
}
