# Maput

## Installation

 - Run : npm install --global expo-cli
 - Donwload phone app (expo-cli)
 - See [documentation](https://docs.expo.dev/get-started/installation/)
 - ask for .env

## Run project

- In the project directory : expo start

## To Do
- [ ] close windows in map
- [ ]  windows detail for each place
- [ ] login system ( perhaps connect it to UTC ?)
- [ ] made it beautiful
- [ ] photo system for each place
- [ ] selection system ( less than price, higher than price blablabla)

##Backend
###Installation
- Avoir Nodejs version 16+
- npm i -g @nestjs/cli
- Dans le dossier du backend : npm install
- npm run start pour lancer le serveur

###Base de donnée
Le serveur a besoin de pouvoir se connecter à une base de donnée MongoDB en localhost :
- Installer MongoDB
- S'assurer d'avoir une base au port 27017
- La nommer map_ut
